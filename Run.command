#!/usr/bin/env bash
set -euo pipefail

# Created Date: Thursday, June 20th 2021, 10:46:53 am
# Author: Sergey A. Kolesnikov

target="Importer.py"
dir=$(pwd)

if [[ $EUID = 0 ]]; then
    printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
    exit 1
fi

command_exists() {
    command -v "$@" >/dev/null 2>&1
}

_main() {
    if ! command_exists python3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -y -qq install python3
    fi
    if ! command_exists pip3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -y -qq install python3-pip
    fi
    pip3 install pyjson python-dotenv requests requests-toolbelt
    python3 "$dir/$target"
}

_main
