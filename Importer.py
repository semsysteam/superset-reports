#!/usr/bin/env python3
'''
Created Date: Thursday, June 24th 2021, 10:54:04 am
Author: Sergey A. Kolesnikov
Copyright (c) 2021
'''
import os
import json
import shutil
import requests
from dotenv import load_dotenv
from requests import Request, Session
from requests_toolbelt import MultipartEncoder


class DataUploader:
    def __init__(self):
        dotenv_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), ".env")
        load_dotenv(dotenv_path)
        self.base_url = os.environ.get("SUPERSET_URL")
        self.login = os.environ.get("SUPERSET_USERNAME")
        self.password = os.environ.get("SUPERSET_PASSWORD")
        self.db_alias = os.environ.get("SOURCE_DB_ALIAS")
        self.db_user_name = os.environ.get("SOURCE_DB_USER")
        self.db_user_password = os.environ.get("SOURCE_DB_PASSWORD")
        self.db_host = os.environ.get("SOURCE_DB_HOST")
        self.db_port = os.environ.get("SOURCE_DB_PORT")
        self.db_name = os.environ.get("SOURCE_DB")
        self.reports_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), "reports")
        self.out_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), "out")
        self.reports_out_path = os.path.join(self.out_path, "reports")
        self.json_extension = ".json"
        self.access_token = ""
        self.headers = {}
        self.session = Session()

    # Декодируем json c ascii символами в utf-8
    def _decode_json_files(self, source_path, dest_path):
        json_ext = self.json_extension
        for source_file in os.listdir(source_path):
            if source_file.endswith(json_ext):
                with open(os.path.join(source_path, source_file), encoding='utf-8') as json_file:
                    data = json.load(json_file)
                    with open(os.path.join(dest_path, source_file), 'w') as dest_file:
                        json.dump(data, dest_file, ensure_ascii=False)

    # Получаем Bearer token для авторизации
    def _get_access_token(self):
        login_url = self.base_url + "/api/v1/security/login"
        auth_data = json.dumps({'password': self.password,
                                'provider': 'db',
                                'refresh': False,
                                'username': self.login})
        self.headers = {'accept': 'application/json',
                        'Content-Type': 'application/json; charset=utf-8'}

        auth_resp = requests.post(url=login_url,
                                  headers=self.headers, data=auth_data, verify=False)
        if auth_resp.status_code == 200:
            return auth_resp
        else:
            return None

    # Получаем token от межсайтовой подделки запроса
    def _get_csrftoken(self):
        csrf_url = self.base_url + "/api/v1/security/csrf_token/"
        token_resp = self._get_access_token()
        cookies = token_resp.cookies
        jdict = json.loads(token_resp.content)

        if "access_token" in jdict:
            self.access_token = jdict['access_token']
            self.headers = {'accept': 'application/json',
                            'Content-Type': 'application/json; charset=utf-8', 'Authorization': '{0} {1}'.format('Bearer', self.access_token)}
            csrf_resp = requests.get(
                url=csrf_url, headers=self.headers, cookies=cookies)
            if csrf_resp.status_code == 200:
                return csrf_resp
            else:
                return None

    # Добавляем сведения о соединении с БД
    def _create_db_entry(self):
        create_db_url = self.base_url + "/api/v1/database/"
        db_alias = self.db_alias
        db_user_name = self.db_user_name
        db_user_password = self.db_user_password
        db_host = self.db_host
        db_port = self.db_port
        db_name = self.db_name
        db_connection_string = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
            db_user_name, db_user_password, db_host, db_port, db_name)

        create_db_str = {'allow_csv_upload': False,
                         'allow_ctas': False,
                         'allow_cvas': False,
                         'allow_dml': False,
                         'allow_multi_schema_metadata_fetch': True,
                         'allow_run_async': True,
                         'cache_timeout': 60,
                         'configuration_method': 'sqlalchemy_form',
                         'database_name': db_alias,
                         'encrypted_extra': "",
                         'engine': "",
                         'expose_in_sqllab': True,
                         'extra': "",
                         'force_ctas_schema': "",
                         'impersonate_user': False,
                         'parameters': {
                             'metadata_params': {},
                             'engine_params': {},
                             'metadata_cache_timeout': {},
                             'schemas_allowed_for_csv_upload': []
                         },
                         'server_cert': "",
                         'sqlalchemy_uri': db_connection_string}

        create_db_json = json.dumps(create_db_str)

        try:
            with self.session as s:
                csrftoken_resp = self._get_csrftoken()
                jdict = json.loads(csrftoken_resp.content)
                if "result" in jdict:
                    csrftoken = jdict['result']
                    headers = self.headers
                    headers.update({'X-CSRFToken': csrftoken})
                    cookies = csrftoken_resp.cookies
                    req = Request(method='POST', url=create_db_url,
                                  headers=headers, cookies=cookies)

                    req.data = create_db_json
                    prep = s.prepare_request(req)
                    resp = s.send(prep)
                    if resp.status_code == 201 or resp.status_code == 422:
                        return True
                    else:
                        return False
        except:
            raise

    # Импортируем дашборды
    def _import_dashboards(self, source_path):
        import_dashboard_url = self.base_url + "/api/v1/dashboard/import/"
        if source_path != "" and os.path.exists(source_path):
            try:
                multipart = MultipartEncoder(fields={'overwrite': 'true', 'password': '', 'formData': (source_path,
                                                                                                       open(source_path, 'rb'), 'type=application/json')})
                with self.session as s:
                    csrftoken_resp = self._get_csrftoken()
                    jdict = json.loads(csrftoken_resp.content)
                    if "result" in jdict:
                        csrftoken = jdict['result']
                        headers = self.headers
                        headers.update({'X-CSRFToken': csrftoken})

                        if "Content-Type" in headers:
                            headers['Content-Type'] = multipart.content_type
                        else:
                            headers.update(
                                {'Content-Type', multipart.content_type})

                        cookies = csrftoken_resp.cookies

                        req = Request(method='POST', url=import_dashboard_url,
                                      headers=headers, cookies=cookies, data=multipart)
                        s.verify = False
                        prepped = s.prepare_request(req)

                        resp = s.send(prepped)

                        if resp.status_code == 200:
                            return True
                        else:
                            resp.raise_for_status()
            except:
                raise

    def main(self):
        try:
            json_ext = self.json_extension

            if os.path.exists(self.reports_path):
                if os.path.exists(self.reports_out_path):
                    shutil.rmtree(self.reports_out_path,
                                  ignore_errors=False, onerror=None)
            try:
                os.makedirs(self.reports_out_path, exist_ok=True)
            except OSError as err:
                if err.errno != 17:
                    raise
            try:
                self._decode_json_files(
                    self.reports_path, self.reports_out_path)
            except:
                raise

            if self.db_alias != "" and self.db_host != "" and self.db_user_name != "" and self.db_user_password != "" and self.db_name != "" and self.db_port is not None:
                if self._create_db_entry():
                    for _, _, files in os.walk(self.reports_path):
                        for file in files:
                            if file.endswith(json_ext):
                                self._import_dashboards(
                                    os.path.join(self.reports_out_path, file))
        finally:
            shutil.rmtree(self.out_path, ignore_errors=True)


d = DataUploader()
d.main()
